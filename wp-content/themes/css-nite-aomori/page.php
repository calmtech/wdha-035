<?php
/**
 * ページ
 *
 * @package WordPress
 * @subpackage wp-theme-scaffold
 */
?>
<?php get_header(); ?>
<?php if (have_posts()) : ?>  
    <?php while (have_posts()) : the_post(); ?>  
        <?php the_content('Read the rest of this entry »'); ?>  
    <?php endwhile; ?>  
    <?php else : ?>  
    //Something that happens when a post isn’t found.  
<?php endif; ?>
<p class="totoppage"><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/totoppage.gif" alt="トップページへ" /></a></p>
<?php get_footer(); ?>