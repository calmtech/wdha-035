<?php
/**
 * 新着情報
 *
 * @package WordPress
 * @subpackage wp-theme-scaffold
 */
?>
<?php get_header(); ?>
<h2>新着情報</h2>
<div id="blogindex">
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
	<div class="entry">
        <div class="blogheader">
        	<h3><?php the_title(); ?></h3>
        	<p class="meta">投稿日 <?php the_time('Y/m/d'); ?></p>
       	</div>
       	<div class="smallbody">
       		<?php the_excerpt(); ?>
       		<div class="more"><a href="<?php the_permalink(); ?>">すべての内容を読む &raquo;</a></div>
       	</div>
	</div>
	<hr class="entry-devider" />
    <?php endwhile; ?> 
    
    <?php else : ?>  
    //Something that happens when a post isn’t found.  
<?php endif; ?>
</div>
<p class="totoppage"><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/totoppage.gif" alt="トップページへ" /></a></p>
<?php get_footer(); ?>