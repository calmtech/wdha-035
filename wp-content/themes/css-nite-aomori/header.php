<?php
/**
 * ヘッダー
 *
 * @package WordPress
 * @subpackage wp-theme-scaffold
 */
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<title>CSS Nite in AOMORI, Vol.7｜青森県のホームページ制作・Web担当者向けセミナー :: ホーム</title>
<meta name="description" content="" />

<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
<style type="text/css"> 
#blockStyle22127 {background-repeat:no-repeat; margin:0 0 0 30px; } 
#blockStyle221Layout1Cell127 {background-repeat:no-repeat; margin:0 0 0 30px; } 
#blockStyle417Main30 {background-repeat:no-repeat; margin:60px 0 0 0; } 
#blockStyle122Main23 {background-repeat:no-repeat; } 
#blockStyle123Main22 {background-repeat:no-repeat; margin:0 0 50px 0; } 
#blockStyle401Main20 {background-repeat:no-repeat; } 
#blockStyle404Main21 {background-repeat:no-repeat; } 
</style>

</head>

<body>
	<header>
		<div class="inner">
			<div class="form">
				&nbsp;
			</div>
			<h1><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/toplogo.png" alt="CSS nite in AOMORO vol.7" /></a></h1>
			<p>CSS Nite in AOMORIは、Web制作に関わる方のためのセミナーイベント<br />
			CSS Nite（シーエスエス・ナイト）の青森県版です。<br />
			参加するにあたって資格などはありません。<br />
			Webに興味のあるさまざまな方々の参加をお待ちしています。→ <a href="http://cssnite.jp/about/index.html" alt="CSS Niteについて" target="_blank" />CSS Niteについて</a></p>
		</div>
		<div class="nav">
			<nav>
				<ul>
					<li><a href="#summary"><img src="<?php bloginfo('template_url'); ?>/img/nav/01.gif" alt="開催概要" /></a></li>
					<li><a href="#session"><img src="<?php bloginfo('template_url'); ?>/img/nav/02.gif" alt="出演者・セッション" /></a></li>
					<li><a href="#time_table"><img src="<?php bloginfo('template_url'); ?>/img/nav/03.gif" alt="タイムテーブル" /></a></li>
					<li><a href="<?php bloginfo('url'); ?>/access"><img src="<?php bloginfo('template_url'); ?>/img/nav/04.gif" alt="会場アクセス" /></a></li>
				</ul>
			</nav>
		</div>
	</header>


	<div class="wrapper">
	<?php if(is_home()) : ?>
		<article id="visual">
			<div style="width:40%;float: left;">
				<p><img src="<?php bloginfo('template_url'); ?>/img/topvis-1.gif" alt="" width="361" height="199" /></p>
			</div>
			<div style="width:59.99%;float: left;">
				<img border="0" class="ccm-image-block" alt="" src="<?php bloginfo('template_url'); ?>/img/topvis-2.jpg" width="561" height="133" />
			</div>
			<div class="ccm-spacer"></div>
		</article>
	<?php endif; ?>

		<div id="mainwrapper">
			<div id="main">