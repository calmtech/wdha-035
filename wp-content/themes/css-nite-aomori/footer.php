<?php
/**
 * フッター
 *
 * @package WordPress
 * @subpackage wp-theme-scaffold
 */
?>
		</div><!-- #main -->
	</div><!-- #mainwrapper -->
	<?php get_sidebar(); ?>	
	</div><!-- .wrapper -->

	<footer>
		<div class="inner">
			<ul class="col1">
				<li><a title="ホーム" href="index.html">CSS Nite in AOMORI ホーム</a></li>
				<li><a title="新着情報" href="notice">新着情報</a></li>
				<li><a title="ホーム" href="#summary">開催概要</a></li>
				<li><a title="出演者・セッション" href="#session">出演者・セッション</a></li>
				<li><a title="タイムテーブル" href="#time_table">タイムテーブル</a></li>
				<li><a title="会場アクセス" href="/vol07/access.html">会場アクセス</a></li>
			</ul>
			<ul class="col2">
				<li><a title="協賛について" href="sponsor.html">協賛について</a></li>
				<li><a title="バナー" href="banner.html">バナー</a></li>
				<li><a title="よくあるご質問" href="faq.html">よくあるご質問</a></li>
				<li><a title="運営者情報" href="management.html">運営者情報</a></li>
				<li><a title="協力団体" href="cooperate.html">協力団体</a></li>
				<li><a title="プライバシーポリシー" href="privacy.html">プライバシーポリシー</a></li>
			</ul>
			<ul class="col3">
				<li><a href="http://cssnite.jp/about/index.html" target="_blank">CSS Nite について</a></li>
				<li><a href="https://docs.google.com/forms/d/19JxRxjIGzyLQShu6SiGCUlcsqnR_2lEWOT9N2O4bj_g/viewform">お問い合わせ</a></li>
			</ul>
			<div id="blockStyle417Main30" class=" ccm-block-styles" >
				<p> </p>
				<p class="copyright">Copyright <span style="font-family: 'Times New Roman', 'Times';">©</span>2013 CSS Nite AOMORI. All Rights Reserved.</p>
			</div>
			<p class="end"> </p>
		</div>
	</footer>
</body>
</html>