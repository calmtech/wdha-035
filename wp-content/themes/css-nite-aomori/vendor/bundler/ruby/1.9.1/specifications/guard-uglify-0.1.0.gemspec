# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "guard-uglify"
  s.version = "0.1.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Aaron Cruz"]
  s.date = "2011-05-13"
  s.description = "guard file for the uglifier gem"
  s.email = ["aaron@aaroncruz.com"]
  s.homepage = "http://aaroncruz.com"
  s.require_paths = ["lib"]
  s.rubyforge_project = "guard-uglify"
  s.rubygems_version = "1.8.24"
  s.summary = "guard file for the uglifier gem"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<guard>, [">= 0.2.2"])
      s.add_runtime_dependency(%q<uglifier>, [">= 0"])
    else
      s.add_dependency(%q<guard>, [">= 0.2.2"])
      s.add_dependency(%q<uglifier>, [">= 0"])
    end
  else
    s.add_dependency(%q<guard>, [">= 0.2.2"])
    s.add_dependency(%q<uglifier>, [">= 0"])
  end
end
