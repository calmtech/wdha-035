# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "guard-concat"
  s.version = "0.0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Francesco 'makevoid' Canessa"]
  s.date = "2013-01-23"
  s.description = "    Guard::Concat automatically concatenates files in one when watched files are modified.\n"
  s.email = "makevoid@gmail.com"
  s.homepage = "http://github.com/makevoid/guard-concat"
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.24"
  s.summary = "Guard gem for concatenating (js/css) files"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<guard>, [">= 1.1.0"])
    else
      s.add_dependency(%q<guard>, [">= 1.1.0"])
    end
  else
    s.add_dependency(%q<guard>, [">= 1.1.0"])
  end
end
