<?php
/**
 * シングル
 *
 * @package WordPress
 * @subpackage wp-theme-scaffold
 */
?>
<?php get_header(); ?>
<div id="entry">
<?php if (have_posts()) : ?>  
    <?php while (have_posts()) : the_post(); ?>
	<div class="blogheader"></div>
	<div class="head">
		<h2><?php the_title(); ?></h2>
		<p class="meta">更新日：<?php the_time("Y/m/d"); ?></p>
	</div>
	<div class="body">
		<?php the_content(); ?>
	</div>
	<div class="blogfooter">
		<hr/>
		<div id="ccm-next-previous-206" class="ccm-next-previous-wrapper">
			<div class="ccm-next-previous-previouslink">
				<?php previous_post('%', '&laquo; 前', 'no', 'yes'); ?>
			</div>
			<div class="ccm-next-previous-parentlink">
				<a href="<?php echo get_category_link(get_cat_ID('新着情報')); ?>">お知らせ一覧へ</a>
			</div>
			<div class="ccm-next-previous-nextlink">
				<?php next_post('%', '次 &raquo;', 'no', 'yes'); ?>
			</div>
			<div class="spacer"></div>
		</div>
	</div>
    <?php endwhile; ?>  
    <?php else : ?>  
    //Something that happens when a post isn’t found.  
<?php endif; ?>  
</div>
<?php get_footer(); ?>