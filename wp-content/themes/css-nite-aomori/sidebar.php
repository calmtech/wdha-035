<?php
/**
 * サイドバー
 *
 * @package WordPress
 * @subpackage wp-theme-scaffold
 */
?>
<div id="sidebar">

	<ul class="snsb">
		<li>
			<div class="fb-like" data-href="http://cssnite.studiomd.jp/vol07/" data-send="false" data-layout="box_count" data-width="70" data-show-faces="true"> </div>
		</li>
		<li><a class="twitter-share-button" href="https://twitter.com/share" data-url="http://cssnite.studiomd.jp/vol07/" data-text="CSS Nite in AOMORI, Vol.7" data-lang="ja" data-hashtags="cssnite_aomori7" data-count="vertical">ツイート</a></li>
	</ul>

	<div id="blockStyle401Main20" class="sponsors ccm-block-styles" >
		<h2>ご協賛</h2>
		<ul>
			<li><img src="<?php bloginfo('template_url'); ?>/img/sb_dummy.png" width="180" height="80" /></li>
			<li><img src="<?php bloginfo('template_url'); ?>/img/sb_dummy.png" width="180" height="80" /></li>
			<li><img src="<?php bloginfo('template_url'); ?>/img/sb_dummy.png" width="180" height="80" /></li>
		</ul>
		<p><a title="協賛について" href="/vol07/sponsor/">ご協賛方法</a></p>
	</div>

	<div id="blockStyle404Main21" class="sponsors ccm-block-styles" >
		<h2>プレゼント協賛</h2>
		<ul>
			<li><img src="<?php bloginfo('template_url'); ?>/img/ps_dummy.png" width="180" height="80" /></li>
			<li><img src="<?php bloginfo('template_url'); ?>/img/ps_dummy.png" width="180" height="80" /></li>
		</ul>
		<p>募集中です。</p>
	</div>
	
	<div class="notice" >
		<h2>新着情報</h2>
		<?php $posts =  get_posts(array('category_name' => 'notice', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 5)); ?>
		<ul>
		<?php foreach ($posts as $key => $post) : setup_postdata($post); ?>
			<li><p><a href="<?php the_permalink(); ?>">
				<?php the_title(); ?><br /></a>
				<span class="date">[<?php the_time('Y.m.d'); ?>]</span>
				</p>
			</li>
		<?php endforeach; ?>
		</ul>
	</div>

	<div id="blog-index-foot">
	</div>

	<div id="HTMLBlock141" class="HTMLBlock">
		<div class="fb">
			<h2>Facebookページ</h2>
			<div class="fb-like-box">
				<img src="<?php bloginfo('template_url'); ?>/img/fb_dummy.png" />
			</div>
		</div>
	</div>

	<div id="blockStyle122Main23" class="archive ccm-block-styles" >
		<h2>アーカイブ</h2>
		<ul>
			<li><a href="http://cssnite.studiomd.jp/vol06/">CSS Nite in AOMORI, Vol.6</a></li>
			<li><a href="http://cssnite.studiomd.jp/vol05/">CSS Nite in AOMORI, Vol.5</a></li>
			<li><a href="http://cssnite.studiomd.jp/vol04/">CSS Nite in AOMORI, Vol.4</a></li>
			<li><a href="http://cssnite.studiomd.jp/vol03/">CSS Nite in AOMORI, Vol.3</a></li>
			<li><a href="http://cssnite.studiomd.jp/vol02/">CSS Nite in AOMORI, Vol.2</a></li>
			<li><a href="http://cssnite.studiomd.jp/vol01/">CSS Nite in AOMORI, Vol.1</a></li>
		</ul>
	</div>
</div>