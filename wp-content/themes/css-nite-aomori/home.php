<?php
/**
 * ホーム
 *
 * @package WordPress
 * @subpackage wp-theme-scaffold
 */
?>
<?php get_header(); ?>
<div id="blockStyle43532" class=" ccm-block-styles" >
	<h2>CSS Nite in AOMORI, Vol.7「プロの仕事から考えるコンテンツの活かし方」を開催、無事終了しました</h2>
	<p>4月27日（土）に、CSS Nite in AOMORI, Vol.7「プロの仕事から考えるコンテンツの活かし方」を開催、無事終了しました。<br />参加いただいたみなさん、応援していただいたみなさん、ありがとうございました！</p>
	<p> </p>
	<p><img src="<?php bloginfo('template_url'); ?>/photo/IMG_8090m.jpg" alt="IMG_8090m.jpg" width="719" height="478" /></p>
</div>

<p><a name="summary"></a></p>
<h2>開催概要</h2>
<p> すごく久々ですが、レギュラーミーティングやります。<br />諸般の事情により第3土曜日でなく第2土曜日です。スミマセン。</p>
<dl>
	<dt>イベント名</dt><dd>WDHAレギュラーミーティング#035<br />CMSからサイト設計を考える - concrete5 & WordPress</dd>
	<dt>日時　</dt><dd>2013年7月13日（土曜日） 14:00～18:00</dd>
	<dt>会場　</dt><dd>県民福祉プラザ 4F 小研修室（青森市）</dd>
	<dt>定員　</dt><dd>20名</dd>
	<dt>参加費</dt><dd> 500円（場所代として）</dd>
	<dt>出演　</dt><dd>○古川　勝也<br /> 　　CMS導入前のサイト設計<br />○橋本　英樹<br /> 　　concrete5に組み込む場合<br />○古川　勝也<br /> 　　WordPressに組み込む場合</dd>
</dl>

<p><a name="session"></a></p>
<h2>出演者・セッション</h2>

<div class="session">
	<span class="category">セッション：</span>
	<h3>CMS導入前のサイト設計 &amp; WordPressに組み込む</h3>

	<p><span>静的ウェブページをWordPressに組み込む場合の基本的な考え方や、実際にWordPressへの組み込み行程のデモンストレーションを行います。</span></p>
	<div class="profile">
		<div class="photo">
			<img src="<?php bloginfo('template_url'); ?>/photo/dummy.gif" width="180" height="180" alt="" />
		</div>
		<h4>古川　勝也<span class="ruby">（こがわ・まさや）</span></h4>
		<hr />
		<p><a href="http://www.calmtech.net/" target="_blank">カームテック</a> 代表<br><a href="http://www.aoit.jp" target="_blank">NPO法人 あおもりIT活用サポートセンター</a> 理事</p>
		<p>青森県出身、弘前市を拠点にフリーランスのプログラマーとして様々な業種と関わりながらシステム開発を行う。基幹業務システム開発の経験を経て独立後、テクニカルディレクション、システム開発を中心に活動。情報設計、マーケティング、フロントエンド開発、バックエンド開発と職種を問わず、全プロセスをこなして問題解決に取り組む。Ruby勉強会＠青森の運営員。</p>
	</div>
</div>


<div class="session">
	<span class="category">セッション：</span>
	<h3>concrete5に組み込む</h3>

	<p><span>静的ウェブページをconcrete5に組み込む場合の基本的な考え方や、実際にconcrete5のテーマの作成のデモンストレーションを行います。</span></p>
	<div class="profile">
		<div class="photo">
			<img src="<?php bloginfo('template_url'); ?>/photo/dummy.gif" width="180" height="180" alt="" />
		</div>
		<h4>橋本　英樹<span class="ruby">（はしもと・ひでき）</span></h4>
		<hr />
		<p><a href="http://usagi-project.org/" target="_blank">UsagiProject</a> concrete5日本語チーム</p>
		<p>オープンソースCMS「concrete5」の日本語翻訳、日本語版開発・デバックに参加。<br>開発チーム内では、開発環境を公開環境に適応させるためのPHPショートタグ変換ツールを作成提供。</p>
		<p>concrte5日本語公式サイト <a href="http://concrete5-japan.org/" target="_blank">http://concrete5-japan.org/</a><br>
		毎週木曜日23:00頃から配信されている『<a href="http://concrete5-japan.org/news/weekly-concrete5/" target="_blank"> 週刊concrete5 </a> 』に気まぐれに出演中。</p>	
	</div>
</div>

<p><a name="time_table"></a></p>
<h2>タイムテーブル</h2>
<p>タイムテーブルはあくまでも目安であり、当日の流れによって、調整しながら進行します。</p>
<table border="0">
	<thead>
		<tr>
			<td>開始</td>
			<td>セッション</td>
			<td>出演者</td>
			<td>時間</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th>14:00</th>
			<td>開場</td>
			<td> </td>
			<td> </td>
		</tr>
		<tr>
			<th scope="col">14:20</th>
			<td>CMS導入前のサイト設計</td>
			<td>古川勝也</td>
			<td>30分</td>
		</tr>
		<tr>
			<th scope="col">14:50</th>
			<td>concrete5に組み込む場合</td>
			<td>橋本英樹</td>
			<td>60分</td>
		</tr>
		<tr>
			<th scope="col">15:50</th>
			<td>休憩</td>
			<td> </td>
			<td>10分</td>
		</tr>
		<tr>
			<th scope="col">16:00</th>
			<td>WordPressに組み込む場合</td>
			<td>古川勝也</td>
			<td>40分</td>
		</tr>
		<tr>
			<th scope="col">17:00</th>
			<td>総括・質疑応答</td>
			<td> </td>
			<td>30分</td>
		</tr>
		<tr>
			<th scope="col">17:30</th>
			<td>撤収</td>
			<td> </td>
			<td> </td>
		</tr>
	</tbody>
</table>
<div id="HTMLBlock433" class="HTMLBlock"></div>
<?php get_footer(); ?>